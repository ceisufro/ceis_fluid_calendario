<?php
/**
 * Created by PhpStorm.
 * User: Javier
 * Date: 09-05-16
 * Time: 15:25
 */

namespace Francisco\Calendario\ViewHelpers;

use TYPO3\CMS\Fluid\ViewHelpers\Form\AbstractFormViewHelper;
use TYPO3\CMS\Fluid\View\StandaloneView;

class CalendarioViewHelper extends AbstractFormViewHelper {

    /**
     * @param string $fecha
     * @param string $tamano
     * @return string
     */
    public function render ($fecha, $tamano) {

        $directorioVista = PATH_site."typo3conf/ext/calendario/Resources/Private/Templates/VistaCalendario/";
        $rutaCompletaVista = $directorioVista.'calendario.html';
        $vista = new StandaloneView();
        $vista->setFormat("html");
        $vista->setTemplatePathAndFilename($rutaCompletaVista);

        //Si no se ingresa una fecha, se calculara la de hoy
        if (empty($fecha)) {
            $ano = date("Y");
            $vista->assign("ano", $ano);

            //Se obtiene el mes en ingles y se cambia a español
            $mes = date("F");
            if ($mes=="January") $mes="Enero";
            if ($mes=="February") $mes="Febrero";
            if ($mes=="March") $mes="Marzo";
            if ($mes=="April") $mes="Abril";
            if ($mes=="May") $mes="Mayo";
            if ($mes=="June") $mes="Junio";
            if ($mes=="July") $mes="Julio";
            if ($mes=="August") $mes="Agosto";
            if ($mes=="September") $mes="Septiembre";
            if ($mes=="October") $mes="Octubre";
            if ($mes=="November") $mes="Noviembre";
            if ($mes=="December") $mes="Diciembre";
            $vista->assign("mesTexto", $mes);

            $diaNumero = date("d");
            $vista->assign("diaNumero", $diaNumero);

            //Se obtiene el dia de la semana en ingles y se convierte a español
            $diaTexto = date("l");
            if ($diaTexto=="Monday") $diaTexto="Lunes";
            if ($diaTexto=="Tuesday") $diaTexto="Martes";
            if ($diaTexto=="Wednesday") $diaTexto="Miercoles";
            if ($diaTexto=="Thursday") $diaTexto="Jueves";
            if ($diaTexto=="Friday") $diaTexto="Viernes";
            if ($diaTexto=="Saturday") $diaTexto="Sabado";
            if ($diaTexto=="Sunday") $diaTexto="Domingo";
            $vista->assign("diaTexto", $diaTexto);
        } else {
            //La fecha agregada se divide en dia-mes-año y se pasa a un arreglo
            $arregloFecha = explode("-", $fecha, 3);
            $diaNumero = $arregloFecha[0];
            $mes = $arregloFecha[1];
            $ano = $arregloFecha[2];

            //El numero del dia del mes se envia a la vista creada
            $vista->assign("diaNumero", $diaNumero);

            //LOs meses en numero se conierten textos en español
            if ($mes=="01") $mesTexto="Enero";
            if ($mes=="02") $mesTexto="Febrero";
            if ($mes=="03") $mesTexto="Marzo";
            if ($mes=="04") $mesTexto="Abril";
            if ($mes=="05") $mesTexto="Mayo";
            if ($mes=="06") $mesTexto="Junio";
            if ($mes=="07") $mesTexto="Julio";
            if ($mes=="08") $mesTexto="Agosto";
            if ($mes=="09") $mesTexto="Septiembre";
            if ($mes=="10") $mesTexto="Octubre";
            if ($mes=="11") $mesTexto="Noviembre";
            if ($mes=="12") $mesTexto="Diciembre";

            //El texto del mes y año se envian a la vista
            $vista->assign("mesTexto", $mesTexto);
            $vista->assign("ano", $ano);

            //Se concatenan el año, mes y dia y se usa una funcion para calcular el dia de la semana
            $fechaConc = ($ano."-".$mes."-".$diaNumero);
            $diaTexto = $this->obtenerDiaSemana($fechaConc);

            //El dia de la semana se envia a la vista
            $vista->assign("diaTexto",$diaTexto);
        }

        //Los porcentajes se convierten en decimales que permiten escalar la imagen con css
        if ($tamano=="10%") $escala="0.1";
        if ($tamano=="20%") $escala="0.2";
        if ($tamano=="30%") $escala="0.3";
        if ($tamano=="40%") $escala="0.4";
        if ($tamano=="50%") $escala="0.5";
        if ($tamano=="60%") $escala="0.6";
        if ($tamano=="70%") $escala="0.7";
        if ($tamano=="80%") $escala="0.8";
        if ($tamano=="90%") $escala="0.9";
        if ($tamano=="100%") $escala="1";

        //El decimal se envia a la vista y quien lo recibe es la etiqueta style del contenedor
        $vista->assign("tamano",$escala);

        $alto = "";
        $ancho = "";
        switch ($tamano) {
            case "10%":
                $alto = (566*10)/100;
                $ancho = (498*10)/100;

                break;
            case "20%":
                $alto = (566*20)/100;
                $ancho = (498*20)/100;
                break;
            case "30%":
                $alto = (566*30)/100;
                $ancho = (498*30)/100;
                break;
            case "40%":
                $alto = (566*40)/100;
                $ancho = (498*40)/100;
                break;
            case "50%":
                $alto = (566*50)/100;
                $ancho = (498*50)/100;
                break;
            case "60%":
                $alto = (566*60)/100;
                $ancho = (498*60)/100;
                break;
            case "70%":
                $alto = (566*70)/100;
                $ancho = (498*70)/100;
                break;
            case "80%":
                $alto = (566*80)/100;
                $ancho = (498*80)/100;
                break;
            case "90%":
                $alto = (566*90)/100;
                $ancho = (498*90)/100;
                break;
            case "100%":
                $alto = (566*100)/100;
                $ancho = (498*100)/100;
                break;
        }
        $vista->assign("alto",$alto);
        $vista->assign("ancho",$ancho);

        if ($tamano=="10%") $redimension="imgTam01";
        if ($tamano=="20%") $redimension="imgTam02";
        if ($tamano=="30%") $redimension="imgTam03";
        if ($tamano=="40%") $redimension="imgTam04";
        if ($tamano=="50%") $redimension="imgTam05";
        if ($tamano=="60%") $redimension="imgTam06";
        if ($tamano=="70%") $redimension="imgTam07";
        if ($tamano=="80%") $redimension="imgTam08";
        if ($tamano=="90%") $redimension="imgTam09";
        if ($tamano=="100%") $redimension="imgTam10";
        $vista->assign("redimension",$redimension);

        return $vista->render();
    }

    /**
     * @param string $fecha
     * @return string
     */
    public function obtenerDiaSemana($fecha) {
        $fechaObj = new \DateTime($fecha);
        $diaSemana = $fechaObj->format("l");

        switch($diaSemana) {
            case "Sunday": return "Domingo";
            case "Monday": return "Lunes";
            case "Tuesday": return "Martes";
            case "Wednesday": return "Miercoles";
            case "Thursday": return "Jueves";
            case "Friday": return "Viernes";
            case "Saturday": return "Sabado";
        }
    }
}